<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>

	<c:if test="${not empty errCode}">
		<h1>Exception : ${errCode}</h1>
	</c:if>

	<c:if test="${empty errCode}">
		<h1>Input ... /error to check work @ExceptionHandler</h1>
	</c:if>

	<c:if test="${not empty errMsg}">
		<h4>${errMsg}</h4>
	</c:if>
	
</body>
</html>